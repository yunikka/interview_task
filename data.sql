INSERT INTO `company` (`id`, `company_name`, `contact`)
VALUES
	(1, 'Apple', 'Tim Cook'),
	(2, 'Samsung', 'Lee Kun-hee'),
	(3, 'Sony', 'Kaz Hirai');

INSERT INTO `phones` (`id`, `phone_name`, `company_id`, `model_date`, `cost`)
VALUES
	(1, 'iPhone X', 1, '2017-11-03', 90000),
	(2, 'Samsung S8', 2, '2017-02-26', 60000),
	(3, 'Sony Xperia', 3, '2015-09-02', 30000),
	(4, 'IPhone 8', 1, '2017-09-22', 55000);