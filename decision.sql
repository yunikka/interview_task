/* 

Задание №1.

Вывести название фирм и контактные лица. Нужны те фирмы, которые не выпускали телефоны за последний год.

*/


select
    `company_name`,
    `contact`
from `company` where
    `id` in
    /* ниже делаем выборку id компаний, который выпустили телефоны ранее текущего года */
        (
            select 
                `company_id`
            from `phones` where
                YEAR(`model_date`) < YEAR(CURDATE())
        )
;


/* 

Задание №2.

Вывести название и дату выпуска последней выпущенной модели (по каждой фирме).

*/


select
    `phone_name`,
    `model_date`
from `phones` where
    (`company_id`, `model_date`) in
        /* вложенный вопрос нужен лишь для того, что бы сгруппировать по id компании */
        (
            select
                `company_id`,
                 MAX(`model_date`) 
            from `phones`
            GROUP BY `company_id`
        )
;